const express = require('express');
const {ApolloServer} = require('apollo-server-express');

const typeDefs = require('./types/typeDef');
const resolvers = require('./resolvers');
const app = express();
const PORT = process.env.port ? process.env.port : 3001;

const contextFunction = require('./context/context')

const dataObject = {
    dbURL: "test",
    JWT_SECRET: "abcd"
}

const server = new ApolloServer({
    typeDefs,
    resolvers,
    playground: {
        endpoint: '/graphql',
    },
    context: async ({
        req,
        res
    }) => (
        await contextFunction(req.headers, dataObject)
    )
})
server.applyMiddleware({app})

app.listen(PORT, () => {
    console.log(`GraphQL Server is now running on http://localhost:${PORT}/graphql`);
})