const {  gql } = require('apollo-server-express');
const typeDefs = gql`
    type Query {
        test: Boolean
    }

    type Mutation {
        test: Boolean
    }
`

module.exports = typeDefs